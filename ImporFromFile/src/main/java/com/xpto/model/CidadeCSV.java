package com.xpto.model;

import com.opencsv.bean.CsvBindByName;

public class CidadeCSV {

	@CsvBindByName
	private Long ibge_id;
	
	@CsvBindByName
	private String uf;
	
	@CsvBindByName
	private String name;
	
	@CsvBindByName
	private Boolean capital;
	
	@CsvBindByName
	private Double lon;
	
	@CsvBindByName
	private Double lat;
	
	@CsvBindByName
	private String no_accents;
	
	@CsvBindByName
	private String alternative_names;
	
	@CsvBindByName
	private String microregion;
	
	@CsvBindByName
	private String mesoregion;

	public Long getIbge_id() {
		return ibge_id;
	}

	public void setIbge_id(Long ibge_id) {
		this.ibge_id = ibge_id;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getCapital() {
		return capital;
	}

	public void setCapital(Boolean capital) {
		this.capital = capital;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getNo_accents() {
		return no_accents;
	}

	public void setNo_accents(String no_accents) {
		this.no_accents = no_accents;
	}

	public String getAlternative_names() {
		return alternative_names;
	}

	public void setAlternative_names(String alternative_names) {
		this.alternative_names = alternative_names;
	}

	public String getMicroregion() {
		return microregion;
	}

	public void setMicroregion(String microregion) {
		this.microregion = microregion;
	}

	public String getMesoregion() {
		return mesoregion;
	}

	public void setMesoregion(String mesoregion) {
		this.mesoregion = mesoregion;
	}
}
