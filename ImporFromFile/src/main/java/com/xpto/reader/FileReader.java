package com.xpto.reader;

import java.util.List;

import com.xpto.fromFile.FileRepository;
import com.xpto.model.CidadeCSV;

public abstract class FileReader {

	protected abstract FileRepository getFromFile();
	
	public List<CidadeCSV> getCidades(){		
		return getFromFile().todasCidades();
	}	
	
	public List<String> getColunm(String coluna){
		return getFromFile().resultColunm(coluna);
	}
	
	public Integer count() {
		return getFromFile().count();
	}
	
}
