package com.xpto.fromFile;

import java.util.List;

import com.xpto.model.CidadeCSV;

public interface FileRepository {

	public List<CidadeCSV> todasCidades();
	List<String> resultColunm(String coluna);
	Integer count();
	
}
