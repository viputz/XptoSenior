# XptoSenior
- Banco de dados utilizado MySql 5.7
- FlyWay
- SpringBoot
- JDK Oracle 1.8.0_172

# StartUp
#### configuracao do banco
> username=root password=123456

#### Executavel disponivel em ...XptoSenior\target\XptoSenior-1.0.0-SNAPSHOT.jar
      - > executar java -jar XptoSenior-1.0.0-SNAPSHOT.jar, no cmd ou terminal para acompanhar o log, querys

###### porta do TomCat: 8087
1. Ler o arquivo CSV das cidades para a base de dados
   - A aplicação fica encarregada de criar o schema e as tabelas, em seguida ja carrega o arquivo e salva no banco.

# EndPoints
2. Retornar somente as cidades que são capitais ordenadas por nome.
   - http://localhost:8087/cidade?capitais
3. Retornar o nome do estado com a maior e menor quantidade de cidades e a quantidade de cidades.
   - http://localhost:8087/cidade?estadoCidadeMaxMin
4. Retornar a quantidade de cidades por estado.
   - http://localhost:8087/cidade?estadoCidadeCount
5. Obter os dados da cidade informando o id do IBGE.
   - http://localhost:8087/cidade?ibge=3543402
6. Retornar o nome das cidades baseado em um estado selecionado.
   - http://localhost:8087/cidade?estado=SP
7. Permitir adicionar uma nova Cidade.
   - http://localhost:8087/cidade
     - > {
        "ibge_id": 2222 ,
        "name": "vinicius",
        "capital": false,
        "lon": -63.03326928,
        "lat": -9.90846287,
        "no_accents": "Ariquemes",
        "alternative_names": "aaa",
        "uf": "RO",
        "regiao": {
            "id": 2
        }}
     - > O campo ibge_id não pode ser nulo (ibge_id : null).
     - > O id da regiao deve ser informado. 
     - > Id regiao está sendo validado.
 8. Permitir deletar uma cidade.
    - http://localhost:8087/cidade/55
 9. Permitir selecionar uma coluna (do CSV) e através dela entrar com uma string para filtrar. retornar assim todos os objetos que contenham tal string.
    - http://localhost:8087/csv?nomeArquivo=cidades&colunaCsv=no_accents
      - > application/text, o nome do arquivo deve ser informado na URL
      - > o nome do arquivo deve ser informado na URI
      - > a coluna do arquivo deve ser informada na URI
      - > no corpo deve ser informado o filtro
      
 10. Retornar a quantidade de registro baseado em uma coluna. Não deve contar itens iguais.
     - http://localhost:8087/csv?nomeArquivo=cidades&count
       - > application/text, o nome do arquivo deve ser informado na URI
       - > o nome do arquivo deve ser informado na URI
       - > no corpo deve ser informado a coluna do arquivo
 11. Retornar a quantidade de registros total.
     - http://localhost:8087/cidade?total
     
 12. Dentre todas as cidades, obter as duas cidades mais distantes uma da outra com base na localização (distância em KM em linha reta).
     - http://localhost:8087/cidade?distancia
     
 13. Buscar cidade pelo Id
     - http://localhost:8087/cidade/2
