create table cidade (id bigint not null auto_increment, alternative_names varchar(255), is_capital bit, ibge_id bigint not null, lat double precision, lon double precision, name varchar(255), no_accents varchar(255), uf varchar(255), id_region bigint, primary key (id)) engine=InnoDB
create table regiao (id bigint not null auto_increment, meso_region varchar(255), micro_region varchar(255), primary key (id)) engine=InnoDB
alter table cidade add constraint FKs8h7l3a8wmn6cq1xbqfe42qv0 foreign key (id_region) references regiao (id)
