package com.infomendesti.XptoSeniorUI.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.infomendesti.XptoSeniorUI.config.RestUrlConfiguration;
import com.infomendesti.XptoSeniorUI.model.Cidade;

@Service
public class CidadeServiceImpl implements CidadeService {

	@Autowired
	private RestUrlConfiguration configuration;

	@Autowired
	private RestTemplate restTemplate;

	private String uri;

	@PostConstruct
	private void init() {
		this.uri = configuration.getHost().concat(configuration.getPort().toString());
	}
	
	@Override
	public void criar(Cidade cidade) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remover(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void buscarPeloID(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Cidade> listarCapitais() {
		ResponseEntity<List<Cidade>> exchange = restTemplate.exchange(
				uri.concat(configuration.getCidadeUri().getCapitais()), HttpMethod.GET, HttpEntity.EMPTY,
				new ParameterizedTypeReference<List<Cidade>>() {});
		return exchange.getBody();
//		return null;
	}

	@Override
	public Long getTodosRegistros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> listarEstadosCidadesMaxMin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> listarEstadosCidadesCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cidade getByIbgeID(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getByUf(String estado) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Double, Cidade> getMaiorDistancia() {
		// TODO Auto-generated method stub
		return null;
	}

}
