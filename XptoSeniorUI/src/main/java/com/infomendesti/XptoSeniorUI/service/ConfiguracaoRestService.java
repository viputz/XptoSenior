package com.infomendesti.XptoSeniorUI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infomendesti.XptoSeniorUI.config.RestUrlConfiguration;

@Service
public class ConfiguracaoRestService {

	@Autowired
	private RestUrlConfiguration restUrlConfiguration;

	
	public RestUrlConfiguration getRestUrlConfiguration() {
		return restUrlConfiguration;
	}

}
