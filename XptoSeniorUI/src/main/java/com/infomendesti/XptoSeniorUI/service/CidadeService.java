package com.infomendesti.XptoSeniorUI.service;

import java.util.List;
import java.util.Map;

import com.infomendesti.XptoSeniorUI.model.Cidade;

public interface CidadeService {
	
	void criar(Cidade cidade);
	void remover(Long id);
	void buscarPeloID(Long id);
	List<Cidade> listarCapitais();
	Long getTodosRegistros();
	Map<String, String> listarEstadosCidadesMaxMin();
	Map<String, String> listarEstadosCidadesCount();
	Cidade getByIbgeID(Long id);
	Map<String, String> getByUf(String estado);
	Map<Double, Cidade> getMaiorDistancia();
}
