package com.infomendesti.XptoSeniorUI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.infomendesti.XptoSeniorUI.service.CidadeService;
import com.infomendesti.XptoSeniorUI.service.ConfiguracaoRestService;

@Controller
@RequestMapping("/cidade")
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;
	
	@Autowired
	private ConfiguracaoRestService configuracaoRestService;
		
	@GetMapping
	public ModelAndView listarRestServices(ModelMap model) {
		model.addAttribute("cfg",configuracaoRestService.getRestUrlConfiguration().toString());
		model.addAttribute("cidadeUrls", configuracaoRestService.getRestUrlConfiguration().getCidadeUri());
		
		return new ModelAndView("/cidade/Cidade", model);
	}
	
	@GetMapping("/novo")
	public String novo() {		
		return "cidade/CadastroCidade";
	}
	
	@GetMapping("/capitais")
	public ModelAndView ListarCapitais() {
		ModelAndView mv = new ModelAndView("cidade/CidadeList");
		mv.addObject("cidadesCapitais" , cidadeService.listarCapitais());		
		return mv;
	}
}
