package com.infomendesti.XptoSeniorUI.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:rest.properties")
@ConfigurationProperties(prefix = "rest")
public class RestUrlConfiguration {

	// private String host;
	// private Integer port;
	// private String listarTodas;
	// private String deletar;
	// private String buscarPeloId;
	// private String salvar;
	// private String capitais;
	// private String estadosCidadesMaxMin;
	// private String cidadesCount;
	// private String buscarPeloIbge;
	// private String listarCidadesPeloEstado;
	// private String countCidades;

	private CidadeUri cidadeUri;
	private String host;
	private Integer port;

	public CidadeUri getCidadeUri() {
		return cidadeUri;
	}

	public void setCidadeUri(CidadeUri cidadeUri) {
		this.cidadeUri = cidadeUri;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public static class CidadeUri {

		private String listarTodas;
		private String deletar;
		private String buscarPeloId;
		private String salvar;
		private String capitais;
		private String estadosCidadesMaxMin;
		private String cidadesCount;
		private String buscarPeloIbge;
		private String listarCidadesPeloEstado;
		private String countCidades;

		public String getListarTodas() {
			return listarTodas;
		}

		public void setListarTodas(String listarTodas) {
			this.listarTodas = listarTodas;
		}

		public String getDeletar() {
			return deletar;
		}

		public void setDeletar(String deletar) {
			this.deletar = deletar;
		}

		public String getBuscarPeloId() {
			return buscarPeloId;
		}

		public void setBuscarPeloId(String buscarPeloId) {
			this.buscarPeloId = buscarPeloId;
		}

		public String getSalvar() {
			return salvar;
		}

		public void setSalvar(String salvar) {
			this.salvar = salvar;
		}

		public String getCapitais() {
			return capitais;
		}

		public void setCapitais(String capitais) {
			this.capitais = capitais;
		}

		public String getEstadosCidadesMaxMin() {
			return estadosCidadesMaxMin;
		}

		public void setEstadosCidadesMaxMin(String estadosCidadesMaxMin) {
			this.estadosCidadesMaxMin = estadosCidadesMaxMin;
		}

		public String getCidadesCount() {
			return cidadesCount;
		}

		public void setCidadesCount(String cidadesCount) {
			this.cidadesCount = cidadesCount;
		}

		public String getBuscarPeloIbge() {
			return buscarPeloIbge;
		}

		public void setBuscarPeloIbge(String buscarPeloIbge) {
			this.buscarPeloIbge = buscarPeloIbge;
		}

		public String getListarCidadesPeloEstado() {
			return listarCidadesPeloEstado;
		}

		public void setListarCidadesPeloEstado(String listarCidadesPeloEstado) {
			this.listarCidadesPeloEstado = listarCidadesPeloEstado;
		}

		public String getCountCidades() {
			return countCidades;
		}

		public void setCountCidades(String countCidades) {
			this.countCidades = countCidades;
		}

	}

	@Override
	public String toString() {
		return this.host.concat(this.port.toString());
	}

}
