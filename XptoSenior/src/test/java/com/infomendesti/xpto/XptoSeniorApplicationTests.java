package com.infomendesti.xpto;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.infomendesti.xpto.importfrom.openCsv.FileReaderOpenCSV;
import com.xpto.model.CidadeCSV;
import com.xpto.reader.FileReader;


@RunWith(SpringRunner.class)
@SpringBootTest(classes={XptoSeniorApplication.class})
public class XptoSeniorApplicationTests {
	
	private static List<CidadeCSV> cidadesCsv;
	
	private static FileReader fr = null;
	
	@BeforeClass
	public static void init() {
		fr = new FileReaderOpenCSV("cidades.csv");		
		cidadesCsv = fr.getCidades();
	}

	@Test
	public void ListaDeveConterCidadesDoArquivoCSV() {				
		assertNotNull(cidadesCsv);
	}
	

}
