package com.infomendesti.xpto;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.infomendesti.xpto.importfrom.commonscsv.FileReaderCommonsCsv;
import com.xpto.model.CidadeCSV;
import com.xpto.reader.FileReader;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { XptoSeniorApplication.class })
public class TestWithCommonsCsv {

	private static List<CidadeCSV> cidadesCsv;

	private static FileReader fr = null;

	@BeforeClass
	public static void init() {
		fr = new FileReaderCommonsCsv.Builder().setNomeArquivo("cidades.csv").build();
		cidadesCsv = fr.getCidades();
	}
	
	@Test
	public void ListaDeveConterCidadesDoArquivoCSV() {				
		assertNotNull(cidadesCsv);
	}

	/**Aqui eu testo o requisito 9
	 * @param setFiltro -- aqui deve ser informado o valor para ser filtrado na coluna do arquivo
	 * Deve ser informado a coluna como parametro no metodo.
	 */
	@Test
	public void listarColunaCsv() {
	 fr = new FileReaderCommonsCsv.Builder().setNomeArquivo("cidades.csv").setFiltro("sao paulo").build();
	 List<String> colunm = fr.getColunm("no_accents");
	 assertNotNull("deve conter uma lista de String", colunm);
	}
	
	/**Aqui eu testo o requisito 10
	 * @param setColuna - aqui deve ser informado o nome da coluna que consta no arquivo.
	 * 
	 */
	@Test
	public void countColunmDistinct() {
		fr = new FileReaderCommonsCsv.Builder().setNomeArquivo("cidades.csv")
				.setColuna("mesoregion").build();
		Integer count = fr.count();
		
		assertThat(count, greaterThan(0));
		
	}
}
