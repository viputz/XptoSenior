CREATE TABLE regiao (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    micro_region VARCHAR(50),
    meso_region VARCHAR(50)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE cidade (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    ibge_id BIGINT(20),
    name VARCHAR(50),
    is_capital TINYINT,
    lon DECIMAL(10 , 8 ),
    lat DECIMAL(11 , 8 ),
    no_accents VARCHAR(50),
    alternative_names VARCHAR(50),
    uf VARCHAR(25),
    id_region BIGINT(20),
    FOREIGN KEY (id_region) REFERENCES regiao (id)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;