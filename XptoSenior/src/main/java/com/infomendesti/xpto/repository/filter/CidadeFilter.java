package com.infomendesti.xpto.repository.filter;

import com.infomendesti.xpto.model.enumUF;

public class CidadeFilter {

	private Long ibge_id;
	private Boolean capital;
	private enumUF uf;

	public CidadeFilter(Builder builder) {
		this.ibge_id = builder.ibge_id;
		this.capital = builder.capital;
		this.uf = builder.uf;
	}

	public Long getIbge_id() {
		return ibge_id;
	}

	public void setIbge_id(Long ibge_id) {
		this.ibge_id = ibge_id;
	}

	public Boolean getCapital() {
		return capital;
	}

	public void setCapital(Boolean capital) {
		this.capital = capital;
	}

	public enumUF getUf() {
		return uf;
	}

	public void setUf(enumUF uf) {
		this.uf = uf;
	}

	public static class Builder {
		private Long ibge_id;
		private Boolean capital;
		private enumUF uf;

		public Builder setIbge_id(Long ibge_id) {
			this.ibge_id = ibge_id;
			return this;
		}

		public Builder setCapital(Boolean capital) {
			this.capital = capital;
			return this;
		}
		
		public Builder setUf(enumUF uf) {
			this.uf = uf;
			return this;
		}

		public CidadeFilter build() {
			return new CidadeFilter(this);
		}

	}

}
