package com.infomendesti.xpto.repository.filter;

public class RegiaoFilter {

	private String micro;
	private String meso;

	
	public RegiaoFilter(String micro, String meso) {
	
		this.micro = micro;
		this.meso = meso;
	}

	public String getMicro() {
		return micro;
	}

	public void setMicro(String micro) {
		this.micro = micro;
	}

	public String getMeso() {
		return meso;
	}

	public void setMeso(String meso) {
		this.meso = meso;
	}

}
