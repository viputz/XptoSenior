package com.infomendesti.xpto.repository.regiao;

import com.infomendesti.xpto.model.Regiao;
import com.infomendesti.xpto.repository.filter.RegiaoFilter;

public interface RegiaoRepositoryQuery {

	public Regiao filtrar(RegiaoFilter filtro);

}
