package com.infomendesti.xpto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infomendesti.xpto.model.Regiao;
import com.infomendesti.xpto.repository.regiao.RegiaoRepositoryQuery;

public interface RegiaoRepository extends JpaRepository<Regiao, Long>, RegiaoRepositoryQuery {

}
