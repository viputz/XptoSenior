package com.infomendesti.xpto.repository.regiao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import com.infomendesti.xpto.model.Regiao;
import com.infomendesti.xpto.model.Regiao_;
import com.infomendesti.xpto.repository.filter.RegiaoFilter;

public class RegiaoRepositoryQueryImpl implements RegiaoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Regiao filtrar(RegiaoFilter filtro) {
		Regiao r = null;
		try {
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<Regiao> criteria = builder.createQuery(Regiao.class);
			Root<Regiao> root = criteria.from(Regiao.class);

			Predicate[] predicates = getWhere(filtro, builder, root);
			criteria.where(predicates);
			TypedQuery<Regiao> query = manager.createQuery(criteria);
			r = query.getSingleResult();
		} catch (NoResultException e) {
			return r;
		}
		return r;
	}

	private Predicate[] getWhere(RegiaoFilter filtro, CriteriaBuilder builder, Root<Regiao> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getMeso())) {
			predicates.add(builder.equal(root.get(Regiao_.mesoRegiao), filtro.getMeso()));
		}

		if (!StringUtils.isEmpty(filtro.getMicro())) {
			predicates.add(builder.equal(root.get(Regiao_.microRegiao), filtro.getMicro()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
