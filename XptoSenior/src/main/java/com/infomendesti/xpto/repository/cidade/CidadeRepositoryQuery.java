package com.infomendesti.xpto.repository.cidade;

import java.util.List;

import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.repository.filter.CidadeFilter;
import com.infomendesti.xpto.repository.projection.EstadoCidadeCount;

public interface CidadeRepositoryQuery {
	
	public Cidade filtrarCidade(CidadeFilter filtro);
	public List<Cidade> filtrarCidades(CidadeFilter filtro);
	List<EstadoCidadeCount> filtrarEstadosCidadesCount(CidadeFilter filtro);

}
