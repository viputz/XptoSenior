package com.infomendesti.xpto.repository.cidade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.model.Cidade_;
import com.infomendesti.xpto.repository.filter.CidadeFilter;
import com.infomendesti.xpto.repository.projection.EstadoCidadeCount;

public class CidadeRepositoryQueryImpl implements CidadeRepositoryQuery {
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Cidade filtrarCidade(CidadeFilter filtro) {
		Cidade r = null;
		try {
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<Cidade> criteria = builder.createQuery(Cidade.class);
			Root<Cidade> root = criteria.from(Cidade.class);

			Predicate[] predicates = getWhere(filtro, builder, root);
			criteria.where(predicates);
			TypedQuery<Cidade> query = manager.createQuery(criteria);
			r = query.getSingleResult();
		} catch (NoResultException e) {
			return r;
		}
		return r;
	}

	@Override
	public List<Cidade> filtrarCidades(CidadeFilter filtro) {
		List<Cidade> r = null;
		try {
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<Cidade> criteria = builder.createQuery(Cidade.class);
			Root<Cidade> root = criteria.from(Cidade.class);

			Predicate[] predicates = getWhere(filtro, builder, root);
			criteria.where(predicates);
			criteria.orderBy(builder.asc(root.get(Cidade_.name)));
			TypedQuery<Cidade> query = manager.createQuery(criteria);
			r = query.getResultList();
		} catch (NoResultException e) {
			return r;
		}
		return r;
	}

	@Override
	public List<EstadoCidadeCount> filtrarEstadosCidadesCount(CidadeFilter filtro) {
		List<EstadoCidadeCount> r = null;
		try {
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<EstadoCidadeCount> criteria = builder.createQuery(EstadoCidadeCount.class);
			Root<Cidade> root = criteria.from(Cidade.class);

			criteria.select(builder.construct(EstadoCidadeCount.class, builder.count(root.get(Cidade_.ibge_id)),
					root.get(Cidade_.uf)));

			criteria.groupBy(root.get(Cidade_.uf));

			TypedQuery<EstadoCidadeCount> query = manager.createQuery(criteria);
			r = query.getResultList();
		} catch (NoResultException e) {
			return r;
		}
		return r;
	}

	private Predicate[] getWhere(CidadeFilter filtro, CriteriaBuilder builder, Root<Cidade> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (filtro.getIbge_id() != null) {
			predicates.add(builder.equal(root.get(Cidade_.ibge_id), filtro.getIbge_id()));
		}

		if (filtro.getCapital() != null) {
			predicates.add(builder.equal(root.get(Cidade_.capital), filtro.getCapital()));
		}

		if (filtro.getUf() != null) {
			predicates.add(builder.equal(root.get(Cidade_.uf), filtro.getUf()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}
}
