package com.infomendesti.xpto.repository.projection;

import java.util.List;

import com.infomendesti.xpto.model.enumUF;

public class EstadoCidadeCount {

	private enumUF uf;
	private Long countCidades;
	private String nomeEstado;
	private List<String> nomeCidades;
	private String nomeCidade;

	public EstadoCidadeCount() {
	}
	
	public EstadoCidadeCount(Long countCidades, enumUF uf) {
		this.uf = uf;
		this.countCidades = countCidades;
	}

	public enumUF getUf() {
		return uf;
	}

	public void setUf(enumUF uf) {
		this.uf = uf;
	}

	public Long getCountCidades() {
		return countCidades;
	}

	public void setCountCidades(Long countCidades) {
		this.countCidades = countCidades;
	}

	public String getNomeEstado() {
		return nomeEstado;
	}

	public void setNomeEstado(String nomeEstado) {
		this.nomeEstado = nomeEstado;
	}

	public List<String> getNomeCidades() {
		return nomeCidades;
	}

	public void setNomeCidades(List<String> nomeCidades) {
		this.nomeCidades = nomeCidades;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}	
	

}
