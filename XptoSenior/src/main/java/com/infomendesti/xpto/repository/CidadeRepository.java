package com.infomendesti.xpto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.repository.cidade.CidadeRepositoryQuery;

public interface CidadeRepository extends JpaRepository<Cidade, Long>, CidadeRepositoryQuery {

}
