package com.infomendesti.xpto.importfrom.commonscsv;

import com.xpto.fromFile.FileRepository;
import com.xpto.reader.FileReader;

public class FileReaderCommonsCsv extends FileReader {

	private String nomeArquivo;
	private String filtro;
	private String coluna;

	public FileReaderCommonsCsv(Builder builder) {
		this.nomeArquivo = builder.nomeArquivo;
		this.filtro = builder.filtro;
		this.coluna = builder.coluna;
	}

	@Override
	protected FileRepository getFromFile() {
		
		return new ImportarCidadeUsingCommonsCsv.Builder()
				.setColuna(coluna).setFiltro(filtro)
				.setNomeArquivo(nomeArquivo).build();
	}	
	
	public static class Builder {
		private String nomeArquivo;
		private String filtro;
		private String coluna;
		
		public Builder() {
		
		}
		
		public Builder setColuna(String coluna) {
			this.coluna = coluna;
			return this;
		}
		
		public Builder setFiltro(String filtro) {
			this.filtro = filtro;
			return this;
		}
		
		public Builder setNomeArquivo(String nomeArquivo) {
			this.nomeArquivo = nomeArquivo;
			return this;
		}
		
		public FileReaderCommonsCsv build() {
			return new FileReaderCommonsCsv(this);
		}
	}

}
