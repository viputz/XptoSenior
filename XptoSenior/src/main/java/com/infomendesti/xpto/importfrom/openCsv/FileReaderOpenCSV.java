package com.infomendesti.xpto.importfrom.openCsv;

import com.xpto.fromFile.FileRepository;
import com.xpto.reader.FileReader;

public class FileReaderOpenCSV extends FileReader {

	private String nomeArquivo;

	public FileReaderOpenCSV(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	@Override
	protected FileRepository getFromFile() {
		return new ImportarCidadeUsingOpenCSV(nomeArquivo);
	}

}
