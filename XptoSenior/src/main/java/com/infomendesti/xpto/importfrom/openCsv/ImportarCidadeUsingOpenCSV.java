package com.infomendesti.xpto.importfrom.openCsv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.infomendesti.xpto.startup.CarregarArquivotoDb;
import com.opencsv.bean.CsvToBeanBuilder;
import com.xpto.fromFile.FileRepository;
import com.xpto.model.CidadeCSV;

public class ImportarCidadeUsingOpenCSV implements FileRepository {
	
	private final static Logger  logger = LoggerFactory.getLogger(CarregarArquivotoDb.class);

	private String nomeArquivo;

	public ImportarCidadeUsingOpenCSV(String nomearquivo) {
		this.nomeArquivo = nomearquivo;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<CidadeCSV> todasCidades() {
		List<CidadeCSV> cidadeCsv = null;
		try  {
			Reader reader = getFile();
			cidadeCsv = new CsvToBeanBuilder(reader)
					.withType(CidadeCSV.class)
					.withIgnoreLeadingWhiteSpace(true)
					.build()
					.parse();

		} catch (Exception e) {
			logger.error("erro ao Ler o arquivo CSV: {}", e.getCause() != null ? e.getCause() : e.getMessage());
		}

		return cidadeCsv;
	}


	@Override
	public List<String> resultColunm(String coluna) {
		// TODO qualquer outra implementacao aqui utilizando outra lib
		return null;
	}
	
	
	@Override
	public Integer count() {
		// TODO qualquer outra implementacao aqui utilizando outra lib
		return null;
	}
	

	
	private Reader getFile() throws IOException {
		
		 InputStream inputStream = new ClassPathResource(nomeArquivo).getInputStream();
		 Reader reader = new InputStreamReader(inputStream);
		
		return reader;
	}

	

}
