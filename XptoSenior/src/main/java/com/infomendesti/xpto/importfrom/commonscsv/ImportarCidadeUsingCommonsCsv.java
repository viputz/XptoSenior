package com.infomendesti.xpto.importfrom.commonscsv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;

import com.infomendesti.xpto.exception.ArquivoNaoEncontrado;
import com.xpto.fromFile.FileRepository;
import com.xpto.model.CidadeCSV;

public class ImportarCidadeUsingCommonsCsv implements FileRepository {

	@Autowired
	private Logger logger;

	private String nomeArquivo;

	private String filtro;
	
	private String coluna;

	private List<String> list;

	public ImportarCidadeUsingCommonsCsv(Builder builder) {
		this.nomeArquivo = builder.nomeArquivo;
		this.filtro = builder.filtro;
		this.coluna = builder.coluna;
	}

	@Override
	public List<CidadeCSV> todasCidades() {
		List<CidadeCSV> list = new ArrayList<CidadeCSV>();
		try {
			List<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(getFile()).getRecords();
			for (int i = 1; i < records.size(); i++) {
				CSVRecord csvRecord = records.get(i);
				CidadeCSV cidade = new CidadeCSV();
				for (int j = 0; j < csvRecord.size(); j++) {
					cidade.setIbge_id(Long.parseLong(csvRecord.get(j++)));
					cidade.setUf(csvRecord.get(j++));
					cidade.setName(csvRecord.get(j++));
					cidade.setCapital(Boolean.parseBoolean(csvRecord.get(j++)));
					cidade.setLon(Double.parseDouble(csvRecord.get(j++)));
					cidade.setLat(Double.parseDouble(csvRecord.get(j++)));
					cidade.setNo_accents(csvRecord.get(j++));
					cidade.setAlternative_names(csvRecord.get(j++));
					cidade.setMicroregion(csvRecord.get(j++));
					cidade.setMesoregion(csvRecord.get(j++));
				}
				list.add(cidade);
			}

			return list;
		} catch (IOException e) {
			logger.error("erro ao Ler o arquivo CSV");
		}

		return list;
	}

	@Override
	public List<String> resultColunm(String coluna) {
		list = new ArrayList<>();
		try {
			List<CSVRecord> records = CSVFormat.RFC4180.withTrim().withFirstRecordAsHeader().parse(getFile())
					.getRecords();

			for (CSVRecord csvRecord : records) {
				list.add(String.valueOf(csvRecord.get(coluna)));
			}

			if (this.filtro != null || !StringUtils.isEmpty(this.filtro)) {
				list = list.stream().filter(f -> f.toLowerCase().contains(this.filtro.toLowerCase())).collect(Collectors.toList());
			}
			
			return list;

		} catch (IOException e) {
			logger.error("erro ao Ler o arquivo CSV");
		}

		return list;
	}

	@Override
	public Integer count() {
		if (this.coluna != null || !StringUtils.isEmpty(this.coluna)) {
			return count(this.coluna);
		}else {
			return this.todasCidades().size();
		}		 
	}
	
	private Integer count(String coluna) {
		List<String> collect = this.resultColunm(coluna).stream().distinct().collect(Collectors.toList());
		
		return collect.size();
	}


	private Reader getFile() {
		Reader reader = null;
		try {
			InputStream inputStream = new ClassPathResource(nomeArquivo).getInputStream();
			 reader = new InputStreamReader(inputStream);
		} catch (Exception e) {
			throw new ArquivoNaoEncontrado();
		}
		return reader;
	}

	
	public static class Builder {
		private String nomeArquivo;
		private String filtro;
		private String coluna;		
	
		
		public Builder setColuna(String coluna) {
			this.coluna = coluna;
			return this;
		}
		
		public Builder setFiltro(String filtro) {
			this.filtro = filtro;
			return this;
		}
		
		public Builder setNomeArquivo(String nomeArquivo) {
			this.nomeArquivo = nomeArquivo;
			return this;
		}
		
		public ImportarCidadeUsingCommonsCsv build() {
			return new ImportarCidadeUsingCommonsCsv(this);
		}
	}

}
