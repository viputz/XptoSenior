package com.infomendesti.xpto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XptoSeniorApplication {

	public static void main(String[] args) {
		SpringApplication.run(XptoSeniorApplication.class, args);
	}
}
