package com.infomendesti.xpto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infomendesti.xpto.service.ArquivoCsvService;

@RestController
@RequestMapping("/csv")
public class ArquivoCsvResource {

	@Autowired
	private ArquivoCsvService arquivoCsvResource;

	@PostMapping(params = { "nomeArquivo", "colunaCsv" })
	public List<String> buscarNoCsv(@RequestBody String filtro, @RequestParam String colunaCsv,
			@RequestParam String nomeArquivo) {

		return arquivoCsvResource.buscarNoCsv(colunaCsv, filtro, isCsv(nomeArquivo));
	}

	@PostMapping(params = { "nomeArquivo", "count" })
	public Integer count(@RequestParam String nomeArquivo, @RequestBody String coluna) {

		return arquivoCsvResource.count(coluna, isCsv(nomeArquivo));
	}

	private String isCsv(String nomeArquivo) {
		if (!nomeArquivo.trim().endsWith(".csv")) {
			return nomeArquivo.concat(".csv");
		}
		return nomeArquivo;
	}

}
