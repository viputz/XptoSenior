package com.infomendesti.xpto.resource;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.infomendesti.xpto.event.NewResourceEvent;
import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.model.enumUF;
import com.infomendesti.xpto.model.dto.CidadeDto;
import com.infomendesti.xpto.repository.filter.CidadeFilter;
import com.infomendesti.xpto.repository.projection.EstadoCidadeCount;
import com.infomendesti.xpto.service.CidadeService;

@RestController
@RequestMapping("/cidade")
public class CidadeResource {

	@Autowired
	private CidadeService cidadeService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@GetMapping
	public List<Cidade> listarCidades() {
		return cidadeService.listarCidades();
	}

	@PostMapping
	public ResponseEntity<Cidade> criar(@Valid @RequestBody Cidade cidade, HttpServletResponse response) {
		Cidade cidadeEntity = cidadeService.salvar(cidade);
		eventPublisher.publishEvent(new NewResourceEvent(this, response, cidadeEntity.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(cidadeEntity);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		cidadeService.remover(id);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Cidade> buscarPeloId(@PathVariable Long id) {
		Cidade cidade = cidadeService.findById(id);
		return cidade != null ? ResponseEntity.ok(cidade) : ResponseEntity.notFound().build();
	}

	@GetMapping(params = "capitais")
	public List<Cidade> listarCapitais() {
		return cidadeService.listarCapitais();
	}

	@GetMapping(params = "total")
	public ResponseEntity<Long> getTodosRegistros() {
		Long cidades = cidadeService.countCidades();
		return cidades != null ? ResponseEntity.ok(cidades) : ResponseEntity.notFound().build();
	}

	@GetMapping(params = "estadoCidadeMaxMin")
	public List<EstadoCidadeCount> listarEstadosCidadesMaxMin() {
		return cidadeService.listarEstadosCidadesCountMaxMin();
	}

	@GetMapping(params = "estadoCidadeCount")
	public List<EstadoCidadeCount> listarEstadosCidadesCount() {
		return cidadeService.listarEstadosCidadesCount();
	}

	@GetMapping(params = "ibge")
	public ResponseEntity<Cidade> getByIbgeID(@RequestParam Long ibge) {
		Cidade cidade = cidadeService.filtrarCidade(new CidadeFilter.Builder().setIbge_id(ibge).build());
		return cidade != null ? ResponseEntity.ok(cidade) : ResponseEntity.notFound().build();
	}

	@GetMapping(params = "estado")
	public Map<enumUF, List<String>> getByUf(@RequestParam String estado) {

		return cidadeService.filtrarCidades(new CidadeFilter.Builder().setUf(enumUF.getUF(estado)).build());

	}

	@GetMapping(params = "distancia")
	public ResponseEntity<CidadeDto> getMaiorDistancia() {

		CidadeDto cidadeDto = cidadeService.calcularDistancia();

		return cidadeDto != null ? ResponseEntity.ok(cidadeDto) : ResponseEntity.notFound().build();
	}
}
