package com.infomendesti.xpto.exception;

public class FileException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public FileException(String msg) {
	 super(msg);
	 this.msg = msg;
	}
	
	public String getMsg() {
		return msg;
	}

}
