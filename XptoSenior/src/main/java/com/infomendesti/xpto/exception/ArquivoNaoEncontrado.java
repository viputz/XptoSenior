package com.infomendesti.xpto.exception;

public class ArquivoNaoEncontrado  extends FileException{

	private static final long serialVersionUID = 1L;

	public ArquivoNaoEncontrado() {
		super("Nao encontrei o arquivo");
	
	}


}
