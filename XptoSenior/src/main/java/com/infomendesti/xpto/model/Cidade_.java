package com.infomendesti.xpto.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cidade.class)
public abstract class Cidade_ {

	
	public static volatile SingularAttribute<Cidade, String> name;
	public static volatile SingularAttribute<Cidade, enumUF> uf;
	public static volatile SingularAttribute<Cidade, Long> ibge_id ;
	public static volatile SingularAttribute<Cidade, Boolean> capital;
	public static volatile SingularAttribute<Cidade, Long> id;
}
