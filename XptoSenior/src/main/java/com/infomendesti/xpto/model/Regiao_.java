package com.infomendesti.xpto.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Regiao.class)
public abstract class Regiao_ {

	public static volatile SingularAttribute<Regiao, String> microRegiao;
	public static volatile SingularAttribute<Regiao, String> mesoRegiao;
}
