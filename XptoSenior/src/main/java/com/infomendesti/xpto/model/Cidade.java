package com.infomendesti.xpto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.infomendesti.xpto.util.AbstractEntity;

@Entity
@Table(name = "cidade")
public class Cidade extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@NotNull	
	@Column(name = "ibge_id")
	private Long ibge_id;

	@Column(name = "name")
	private String name;

	@Column(name = "isCapital")
	private Boolean capital;

	@Column(name = "lon")
	private Double lon;

	@Column(name = "lat")
	private Double lat;

	@Column(name = "no_accents")
	private String no_accents;

	@Column(name = "alternative_names")
	private String alternative_names;

	@Enumerated(EnumType.STRING)
	private enumUF uf;

	@ManyToOne
	@JoinColumn(name = "id_region")
	private Regiao regiao;

	public Long getIbge_id() {
		return ibge_id;
	}

	public void setIbge_id(Long ibge_id) {
		this.ibge_id = ibge_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getCapital() {
		return capital;
	}

	public void setCapital(Boolean capital) {
		this.capital = capital;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getNo_accents() {
		return no_accents;
	}

	public void setNo_accents(String no_accents) {
		this.no_accents = no_accents;
	}

	public String getAlternative_names() {
		return alternative_names;
	}

	public void setAlternative_names(String alternative_names) {
		this.alternative_names = alternative_names;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	public enumUF getUf() {
		return uf;
	}

	public void setUf(enumUF uf) {
		this.uf = uf;
	}

}
