package com.infomendesti.xpto.model.builder;

import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.model.Regiao;
import com.infomendesti.xpto.model.enumUF;
import com.xpto.model.CidadeCSV;

public class CidadeBuilder {

	private Cidade instance;

	public CidadeBuilder() {
		this.instance = new Cidade();
	}

	public CidadeBuilder setRegiao(String meso, String micro) {
		Regiao r = new Regiao();
		r.setMesoRegiao(meso);
		r.setMicroRegiao(micro);
		this.instance.setRegiao(r);
		return this;
	}

	public CidadeBuilder setFromCsv(CidadeCSV cidadeCSV) {
		this.instance.setAlternative_names(cidadeCSV.getAlternative_names());
		this.instance.setCapital(cidadeCSV.getCapital());
		this.instance.setIbge_id(cidadeCSV.getIbge_id());
		this.instance.setLat(cidadeCSV.getLat());
		this.instance.setLon(cidadeCSV.getLon());
		this.instance.setName(cidadeCSV.getName());
		this.instance.setNo_accents(cidadeCSV.getNo_accents());
		this.instance.setUf(enumUF.valueOf(cidadeCSV.getUf()));
		return this;
	}

	public Cidade build() {
		return this.instance;
	}
}
