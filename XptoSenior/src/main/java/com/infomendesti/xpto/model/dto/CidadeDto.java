package com.infomendesti.xpto.model.dto;

import com.infomendesti.xpto.model.Cidade;

public class CidadeDto {

	private Cidade cidade1;
	private Cidade cidade2;
	private Double distancia;

	

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}	

	
	
	@Override
	public String toString() {
		
		return new StringBuilder()
				.append("As cidades ").append(this.getCidade1().getNo_accents()).append(" e" + this.getCidade2().getNo_accents())
				.append(" possuem a maior ditancia dentre todas as cidades do Brasil, com uma ditancia de ")
				.append(this.distancia.toString()).toString();
			
	
	}

	public Cidade getCidade1() {
		return cidade1;
	}

	public void setCidade1(Cidade cidade1) {
		this.cidade1 = cidade1;
	}

	public Cidade getCidade2() {
		return cidade2;
	}

	public void setCidade2(Cidade cidade2) {
		this.cidade2 = cidade2;
	}

}
