package com.infomendesti.xpto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.infomendesti.xpto.util.AbstractEntity;

@Entity
@Table(name = "regiao")
public class Regiao extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "micro_region")
	private String microRegiao;
	@Column(name = "meso_region")
	private String mesoRegiao;

	public String getMicroRegiao() {
		return microRegiao;
	}

	public void setMicroRegiao(String microRegiao) {
		this.microRegiao = microRegiao;
	}

	public String getMesoRegiao() {
		return mesoRegiao;
	}

	public void setMesoRegiao(String mesoRegiao) {
		this.mesoRegiao = mesoRegiao;
	}

}
