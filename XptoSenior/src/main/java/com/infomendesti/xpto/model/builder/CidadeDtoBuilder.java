package com.infomendesti.xpto.model.builder;

import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.model.dto.CidadeDto;

public class CidadeDtoBuilder {
	
	private CidadeDto instance;

	public CidadeDtoBuilder() {
		this.instance = new CidadeDto();
	}

	public CidadeDtoBuilder setCidade1(Cidade cidade) {
		this.instance.setCidade1(cidade);
		return this;
	}
	
	public CidadeDtoBuilder setCidade2(Cidade cidade) {
		this.instance.setCidade2(cidade);
		return this;
	}
	
	public CidadeDtoBuilder setDistancia(Double cidade) {
		this.instance.setDistancia(cidade);
		return this;
	}

	public CidadeDto build() {
		return this.instance;
	}

}
