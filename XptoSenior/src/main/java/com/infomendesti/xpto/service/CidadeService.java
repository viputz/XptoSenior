package com.infomendesti.xpto.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infomendesti.xpto.importfrom.openCsv.FileReaderOpenCSV;
import com.infomendesti.xpto.model.Cidade;
import com.infomendesti.xpto.model.Regiao;
import com.infomendesti.xpto.model.enumUF;
import com.infomendesti.xpto.model.builder.CidadeBuilder;
import com.infomendesti.xpto.model.builder.CidadeDtoBuilder;
import com.infomendesti.xpto.model.dto.CidadeDto;
import com.infomendesti.xpto.repository.CidadeRepository;
import com.infomendesti.xpto.repository.RegiaoRepository;
import com.infomendesti.xpto.repository.filter.CidadeFilter;
import com.infomendesti.xpto.repository.filter.RegiaoFilter;
import com.infomendesti.xpto.repository.projection.EstadoCidadeCount;
import com.xpto.model.CidadeCSV;
import com.xpto.reader.FileReader;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private RegiaoRepository regiaoRepository;

	private FileReader fileReader;

	public Long countCidades() {
		return cidadeRepository.count();
	}

	public List<Cidade> listarCidades() {
		return cidadeRepository.findAll();
	}

	public List<Cidade> listarCapitais() {
		return cidadeRepository.filtrarCidades(new CidadeFilter.Builder().setCapital(true).build());
	}

	public Cidade salvar(Cidade cidade) {
		return cidadeRepository.save(cidade);
	}

	public void gravarCidadesFromFile() {
		fileReader = new FileReaderOpenCSV("cidades.csv");
		List<CidadeCSV> cidades = fileReader.getCidades();

		if (cidades.size() != this.countCidades().intValue()) {
			cidades.forEach(c -> {
				Cidade cidade = new CidadeBuilder().setFromCsv(c).setRegiao(c.getMesoregion(), c.getMicroregion())
						.build();
				Regiao regiao = regiaoRepository.filtrar(
						new RegiaoFilter(cidade.getRegiao().getMicroRegiao(), cidade.getRegiao().getMesoRegiao()));
				if (regiao != null) {
					cidade.setRegiao(regiao);
				} else {
					regiaoRepository.save(cidade.getRegiao());
				}
				Cidade cidadeSalva = this
						.filtrarCidade(new CidadeFilter.Builder().setIbge_id(cidade.getIbge_id()).build());
				if (cidadeSalva == null) {
					cidadeRepository.save(cidade);
				}

			});
		}
	}

	public Cidade filtrarCidade(CidadeFilter filtro) {
		Cidade cidadeSalva = cidadeRepository.filtrarCidade(filtro);
		return cidadeSalva;
	}

	public void remover(Long id) {
		cidadeRepository.deleteById(id);
	}

	public Cidade findById(Long id) {
		return cidadeRepository.findById(id).orElse(null);
	}

	public List<EstadoCidadeCount> listarEstadosCidadesCount() {

		return getNomeEstado(cidadeRepository.filtrarEstadosCidadesCount(new CidadeFilter.Builder().build()));
	}

	public List<EstadoCidadeCount> listarEstadosCidadesCountMaxMin() {
		List<EstadoCidadeCount> cidadesCount = cidadeRepository
				.filtrarEstadosCidadesCount(new CidadeFilter.Builder().build());

		List<EstadoCidadeCount> list = getNomeEstado(cidadesCount);

		EstadoCidadeCount maior = list.stream()
				.sorted(Comparator.comparing(EstadoCidadeCount::getCountCidades).reversed()).findFirst().orElse(null);

		EstadoCidadeCount menor = list.stream().sorted(Comparator.comparing(EstadoCidadeCount::getCountCidades))
				.findFirst().orElse(null);

		return Arrays.asList(maior, menor);
	}

	public Map<enumUF, List<String>> filtrarCidades(CidadeFilter cidadeFilter) {
		List<Cidade> cidades = cidadeRepository.filtrarCidades(cidadeFilter);

		Map<enumUF, List<String>> map = cidades.stream().collect(
				Collectors.groupingBy(Cidade::getUf, Collectors.mapping(Cidade::getNo_accents, Collectors.toList())));

		return map;
	}

	public CidadeDto calcularDistancia() {
		
		List<Cidade> list = this.cidadeRepository.findAll();
		
		Cidade cidade = list.stream().max(Comparator.comparingDouble(Cidade::getLat)
				.thenComparing(Cidade::getLon)).get();
		
		Cidade cidade2 = list.stream().min(Comparator.comparingDouble(Cidade::getLat)
				.thenComparing(Cidade::getLon)).get();
		
		double calculaDistancia = calculaDistancia(cidade2.getLat(), cidade2.getLon(), cidade.getLat(), cidade.getLon());
		
		
		return new CidadeDtoBuilder().setCidade1(cidade).setCidade2(cidade2).setDistancia(new BigDecimal(calculaDistancia).setScale(2, RoundingMode.UP).doubleValue()).build();
	
	}

	private List<EstadoCidadeCount> getNomeEstado(List<EstadoCidadeCount> cidadesCount) {
		List<EstadoCidadeCount> list = cidadesCount.stream().map(c -> {
			c.setNomeEstado(c.getUf().getNome());
			return c;
		}).collect(Collectors.toList());
		return list;
	}

	private double calculaDistancia(double startLat, double startLong, double endLat, double endLong) {

		Integer curvaturaDaTerra = 6371;

		double dLat = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat = Math.toRadians(endLat);

		double a = potencia(dLat) + Math.cos(startLat) * Math.cos(endLat) * potencia(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return curvaturaDaTerra * c; // <-- d
	}

	public double potencia(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

}
