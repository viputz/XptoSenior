package com.infomendesti.xpto.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.infomendesti.xpto.importfrom.commonscsv.FileReaderCommonsCsv;
import com.xpto.reader.FileReader;

@Service
public class ArquivoCsvService {

	private FileReader fr;
	
	public List<String> buscarNoCsv(String coluna, String filtro, String nomeArquivo){		
		fr = new FileReaderCommonsCsv.Builder().setFiltro(filtro).setNomeArquivo(nomeArquivo).build();
		return fr.getColunm(coluna);
	}
	
	public Integer count(String coluna, String nomeArquivo) {
		fr = new FileReaderCommonsCsv.Builder().setNomeArquivo(nomeArquivo).setColuna(coluna).build();
		return fr.count();
	}
}
