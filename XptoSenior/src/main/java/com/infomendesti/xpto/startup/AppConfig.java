package com.infomendesti.xpto.startup;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.infomendesti.xpto.*")
public class AppConfig {

}
