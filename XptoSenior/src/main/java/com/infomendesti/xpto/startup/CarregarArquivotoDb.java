package com.infomendesti.xpto.startup;

import java.util.Objects;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.infomendesti.xpto.service.CidadeService;

@Component
public class CarregarArquivotoDb {
		
	private final static Logger  logger = LoggerFactory.getLogger(CarregarArquivotoDb.class);

	@Autowired
	private CidadeService cidadeService;
	

	@PostConstruct
	public void init() {
		logger.info("verificando {}", Objects.nonNull(cidadeService));
		this.cidadeService.gravarCidadesFromFile();
		logger.info("verificando depois");
	}
}
